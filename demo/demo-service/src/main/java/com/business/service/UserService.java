package com.business.service;

import com.business.entry.bean.User;

/**
* @author 作者 huangxinyu 
* @version 创建时间：2018年1月13日 上午10:51:00 
* 类说明 
*/

public interface UserService {



	User getUserByOpenId(String openId);



}
